extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _label
var _gdlib

var _p
const EPIC = "Epic"
const STEAM = "Steam"


# Called when the node enters the scene tree for the first time.
func _ready():
	_p = EPIC
	_label = get_node("Label")
	_change_platform()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	_label.set_text(_gdlib.get_platform());


func _on_ChangePlatformButton_pressed():
	_change_platform();

func _change_platform():
	if _p == EPIC :
		_p = "Steam"
	else :
		_p = "Epic"
		
	_gdlib = load("res://bin/GDN" + _p + ".gdns").new()
